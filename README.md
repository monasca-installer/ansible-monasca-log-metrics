# monasca-log-metrics

*monasca-log-metrics* is an application that transforms logs into metrics,
allowing them to be picked up by *monasca* and used in alarming processes.

## Variables

### Required variables
* **zookeeper_hosts** - Example: 192.168.10.4:2181
* **kafka_hosts** - Example: 192.168.10.5:2181

### Optional variables

* **download_timeout** - defines a timeout when downloading ELK tar files (default: 600)
* **download_tmp_dir** - location where logstash is downloaded (default: /tmp)
* **download_force** - set to true to always download logstash archive (default False)
* **logstash_version** - defines the logstash version to be installed (default 2.2.0)
* **logstash_uncompress_dest** - where to unpack logstash archive (default /opt)
* **logstash_dest** - target for symlink that will point to **logstash_uncompress_dest**
* **log_metrics_conf_dir** - a directory where logstash configuration files are stored (default /etc/monasca/log)
* **log_metrics_conf_file** - full path to the logstash configuration file, defaults to '/etc/monasca/log/log-metrics.conf
* **log_metrics_install_user_group** - should install user & group

## Runtime control

* log_metrics_run_mode:
 * Deploy - Full run including: [Install, Configure, Start]
 * Install - Just installation. Configuration, starting is skipped
 * Configure - Just configuration. Installation, starting is skipped
 * Start - Starts the service, includes checking status
 * Stop - Stops the service

* log_metrics_install_mode:
 * tarball - plain installation using logstash tarball and configuration file
 * rpm - uses supplied RPM to perform installation

## Kafka Read/Write

Relevant variable to bear in mind are:
* **log_metrics_source** - dictionary with two keys (topic, group)
* **log_metrics_target** - dictionary with two keys (topic, group)

   Topic meaning depends on context. If that's the source it is a topic to read
   messages from. Otherwise it is a topic aol will write to.

   Group wraps all **log-metric** consumers/producers into single logical entity.

Additionally connectivity options are:
* **log_metrics_id** - common variable for **log_metrics_client_id** and **log_metrics_consumer_id** (default **{{ ansible_hostname }}_log_metrics**)
* **log_metrics_client_id** - producer id (default **log_metrics_id**)
* **log_metrics_consumer_id** - consumer id (default **log_metrics_id**)
* **log_metrics_consumer_threads**
* **log_metrics_client_compression**
* **log_metrics_fetch_msg_max_bytes** - number of bytes of messages to attempt to fetch for each topic-partition in each fetch request (default **1048576**)

## Transformation logic variables

* **log_metrics_detect_log_levels** - provide list of log levels for which
corresponding records should transformed into metrics, empty array is a misconfiguration
and will result in dropping every received log (kafka message).
For log levels that are not specified in **log_metrics_detect_log_levels**
log records will be dropped
* **log_metrics_metric_prefix** - the prefix for metric name. Log's level is appended with (down-case) to this prefix

## Process and resources control (user,group)

The following variables are used to narrow down **monasca-log-metrics** access only to those
resources which it actually needs.

* **log_metrics_user** - user name for **monasca-log-metrics** resources
* **log_metrics_group** - group name for **monasca-log-metrics** resources

## Debugging/Logging
*monasca-log-metrics* writes activity into log file specified by variable **log_metrics_log_dir**.
It is possible to control verbosity of the output with variable **log_metrics_verbosity**.
**log_metrics_verbosity** could contain, for instance **--verbose --debug**.

## Logstash Version

It is possible to control which Logstash will be installed by the mean of
**logstash_version** variable. However, bear in mind that embedded plugins
may have different configurations options, something could have been
deprecated for instance. By default **logstash_version** is *2.2.0*.

## Used plugins
**monasca-log-metrics** is a component that both reads from Kafka and writes
to it. Beware that using older Logstash may require adjustments in
configuration file due to different configuration options in **kafka** plugin.

## License

Apache License, Version 2.0

## Author Information

Tomasz Trebski
